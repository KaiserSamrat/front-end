function validation() {

	var user = document.getElementById('user').value;
	var pass = document.getElementById('pass').value;
	var confirmpass = document.getElementById('conpass').value;
	var mobileNumber = document.getElementById('mobileNumber').value;
	var emails = document.getElementById('emails').value;


	if (user == "") {
	  document.getElementById('username').innerHTML = " ** Please fill the username field";
	  return false;
	}
   

	if (!isNaN(user)) {
	  document.getElementById('username').innerHTML = " ** only characters are allowed";
	  return false;
	}
	if (isNaN(user)) {
	  document.getElementById('username').innerHTML = "";
	  
	}
	if (user != "") {
	  document.getElementById('username').innerHTML = "";
	 
	}

	if (emails == "") {
	  document.getElementById('emailids').innerHTML = " ** Please fill the email id` field";
	  return false;
	}

	if (emails.indexOf('@') <= 0) {
	  document.getElementById('emailids').innerHTML = " ** @ Invalid Email";
	  return false;
	}

	if ((emails.charAt(emails.length - 4) != '.') && (emails.charAt(emails.length - 3) != '.')) {
	  document.getElementById('emailids').innerHTML = " ** . Invalid Email";
	  return false;
	}
	if (emails != "") {
	  document.getElementById('emailids').innerHTML = "";
	 
	}
	if (emails.indexOf('@') > 0) {
	  document.getElementById('emailids').innerHTML = "";
	 
	}
	if ((emails.charAt(emails.length - 4) == '.') && (emails.charAt(emails.length - 3) == '.')) {
	  document.getElementById('emailids').innerHTML = "";
	 
	}


	if (pass == "") {
	  document.getElementById('passwords').innerHTML = " ** Please fill the password field";
	  return false;
	}

	if (pass.length < 8) {
	  document.getElementById('passwords').innerHTML = " ** Minimum 8 digits required";
	  return false;
	}
	if (pass.length > 8) {
	  document.getElementById('passwords').innerHTML = "";
	 
	}
	if (pass != "") {
	  document.getElementById('passwords').innerHTML = "";
	  
	}
	if (confirmpass == "") {
	  document.getElementById('confrmpass').innerHTML = " ** Please fill the confirm password field";
	  return false;
	}
	if (pass != confirmpass) {
	  document.getElementById('confrmpass').innerHTML = " ** Password does not match";
	  return false;
	}

	
	if (pass == confirmpass) {
	  document.getElementById('confrmpass').innerHTML = "";
	  
	}
	if (confirmpass != "") {
	  document.getElementById('confrmpass').innerHTML = "";
	  
	}



	if (mobileNumber == "") {
	  document.getElementById('mobileno').innerHTML = " ** Please fill the mobile NUmber field";
	  return false;
	}
   
	if (isNaN(mobileNumber)) {
	  document.getElementById('mobileno').innerHTML = " ** user must write digits only not characters";
	  return false;
	}
	if (!isNaN(mobileNumber)) {
	  document.getElementById('mobileno').innerHTML = "";
	 
	}
	if (mobileNumber.length != 11) {
	  document.getElementById('mobileno').innerHTML = " ** Mobile Number must be 10 digits only";
	  return false;
	}
	if (mobileNumber.length === 11) {
	  document.getElementById('mobileno').innerHTML = "";
	
	}




  }